﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ZgadywanieLiczby
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Zgadnij liczbę";
            ZgadnijLiczbe gra = new ZgadnijLiczbe();
            gra.Start();
        }
    }
}
class ZgadnijLiczbe
{
    private int max;
    private int min;
    private int zgadywanie;
    private string odpowiedzUzytkownika;
    private int licznik;
    private bool correct;
    private string liczba;


    public void Start()
    {
        Console.Clear();
        PodanieLiczbPrzezUzytkownika();
        WymyslenieLiczby();      
        RozgrywanieGry(min, max);

        if (correct)
        {
            Console.WriteLine("Koniec gry, potrzeba było {0} prób", licznik);

        }
        else
        {
            Console.WriteLine("Koniec gry, wykorzystano 5 prób");
        }
        WybieranieOpcjiPoZakonczeniuGry();
    }
    public void PodanieLiczbPrzezUzytkownika()
    {
        try
        {
            Console.WriteLine("Podaj minimalną liczbę");
            liczba = Console.ReadLine();
            min = Convert.ToInt32(liczba);
            Console.WriteLine("Podaj maksymalną liczbę");
            liczba = Console.ReadLine();
            max = Convert.ToInt32(liczba);
        }
        catch (FormatException)
        {
            Console.WriteLine("Nieprawidłowy wpis. Spróbuj ponownie, wpisując liczbę całkowitą");
            Console.ReadKey(true);
            Start();
        }
        catch (OverflowException)
        {
            Console.WriteLine("Nieprawidłowy zakres. Spróbuj ponownie.");
            Console.ReadKey(true);
            Start();
        }


    }
    public void WymyslenieLiczby()
    {
        Console.WriteLine("Pomyśl o liczbie między {0} a {1}.", min, max);
        Thread.Sleep(3000);
        Console.WriteLine("Gotowy?");
        Console.WriteLine("Naciśnij dowolny klawisz, aby rozpocząć");
        Console.ReadKey(true);
        licznik = 1;
    }

    public void RozgrywanieGry(int min, int max)
    {

        Random rand = new Random();
        while ((!CzyGraSkonczona()))
        {
            zgadywanie = rand.Next(min, max);
            Console.Clear();
            Console.WriteLine("{0}", zgadywanie);
            Console.WriteLine("Czy podana liczba jest prawdiłowa? (T/N)");
            SprawdzenieOdpowiedziUzytkownika();
        }
    }
    public void SprawdzenieOdpowiedziUzytkownika()
    {
        odpowiedzUzytkownika = Console.ReadLine();
        while (odpowiedzUzytkownika != "t" && odpowiedzUzytkownika != "T" && odpowiedzUzytkownika != "n" && odpowiedzUzytkownika != "N")
        {
            Console.WriteLine("Nieprawidłowy wpis, proszę podać 'T' lub 'N'");
            Console.WriteLine("Czy podana liczba jest prawdiłowa? (T/N)");
            odpowiedzUzytkownika = Console.ReadLine();
        }
        if (odpowiedzUzytkownika == "t" || odpowiedzUzytkownika == "T")
        {
            correct = true;
        }
        if (odpowiedzUzytkownika == "n" || odpowiedzUzytkownika == "N")
        {
            licznik++;
            if (max == min)
            {
                Console.WriteLine("Zakres liczb pokrywa się. Naciśnij enter, aby zacząć od nowa");
                Console.ReadKey(true);
                Start();
            }
            Console.WriteLine("Liczba, o której myślisz jest większa czy mniejsza? (W/M)");
            PodawanieSzczegolowOLiczbie();
        }
    }
    public void PodawanieSzczegolowOLiczbie()
    {
        odpowiedzUzytkownika = Console.ReadLine();
        if (odpowiedzUzytkownika != "w" && odpowiedzUzytkownika != "W" && odpowiedzUzytkownika != "m" && odpowiedzUzytkownika != "M")
        {
            Console.WriteLine("Nieprawidłowy wpis, proszę podać 'W' lub 'M'");
            Console.WriteLine("Liczba, o której myślisz jest większa czy mniejsza? (W/M)");
            odpowiedzUzytkownika = Console.ReadLine();
        }
        if (odpowiedzUzytkownika == "M" || odpowiedzUzytkownika == "m")
        {
            max = zgadywanie;
        }
        if (odpowiedzUzytkownika == "W" || odpowiedzUzytkownika == "w")
        {
            min = zgadywanie;
        }
    }
    bool CzyGraSkonczona()
    {
        if (correct)
        {
            return true;
        }

        if (licznik == 6)
        {
            return true;
        }

        return false;
    }

    public void WybieranieOpcjiPoZakonczeniuGry()
    {
        Console.WriteLine("Czy chcesz zagrać ponownie? (T/N)");
        odpowiedzUzytkownika = Console.ReadLine();
        while (odpowiedzUzytkownika != "t" && odpowiedzUzytkownika != "T" && odpowiedzUzytkownika != "n" && odpowiedzUzytkownika != "N")
        {
            Console.WriteLine("Nieprawidłowy wpis, proszę podać 'T', żeby zagrać ponownie lub 'N', jeśli chcesz wyjść");
            Console.WriteLine("Czy chcesz zagrać ponownie? (T/N)");
            odpowiedzUzytkownika = Console.ReadLine();
        }
        if (odpowiedzUzytkownika == "t" || odpowiedzUzytkownika == "T")
        {
            correct = false;
            Start();
        }
        if (odpowiedzUzytkownika == "n" || odpowiedzUzytkownika == "N")
        {
            Environment.Exit(1);
        }
    }
}





